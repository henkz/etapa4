%{
	#include <string.h>
	#include "y.tab.h"
	#include "hash.h"
	#include "astree.h"
	
	#include<stdio.h>
	
	int running = 1;
	int num_linhas = 1;
	int getLineNumber(void)
	{
	 return num_linhas;
	}

	
	char buf[100];
 	char *s=0;
 	

%}


%x COMMENT
%x STRING

%%

FALSE					{ yylval.symbol= hashInsert(yytext,SYMBOL_LIT_FALSE);return LIT_FALSE; }
TRUE					{ yylval.symbol= hashInsert(yytext,SYMBOL_LIT_TRUE);return LIT_TRUE; }
				

int					{ return KW_INT; }
real					{ return KW_REAL; }
bool					{ return KW_BOOL; }
char					{ return KW_CHAR; }
if					{ return KW_IF; }
else					{ return KW_ELSE; }
while					{ return KW_WHILE; }
input					{ return KW_INPUT; }
return					{ return KW_RETURN; }
output					{ return KW_OUTPUT; }


[A-Za-z_][A-Za-z_0-9]*			{ yylval.symbol= hashInsert(yytext,SYMBOL_IDENTIFIER);return TK_IDENTIFIER; }
[0-9]*					{ yylval.symbol= hashInsert(yytext,SYMBOL_LIT_INT);return LIT_INTEGER; }



"<="					{ return OPERATOR_LE; }
">="					{return OPERATOR_GE ;}
"=="					{return OPERATOR_EQ ;}
"!="					{return OPERATOR_NE ;}
"&&"					{return OPERATOR_AND ;}
"||"					{return OPERATOR_OR ;}

[\,\;\:\(\)\[\]\{\}\+\-\*\/\<\>\!\&\$\=] { return yytext[0]; } 

 \" 					{ BEGIN(STRING); s = buf; }
 <STRING>\\n 				{ strcpy(s,"\\n");s=s+2; }
 <STRING>\\t 				{ strcpy(s,"\\t");s=s+2; }
 <STRING>\\\" 				{ *s++ = '\"'; }
 <STRING>\n				{ ++num_linhas; }
 <STRING>\" 				{  *s = 0;  BEGIN(INITIAL); yylval.symbol=hashInsert(buf,SYMBOL_LIT_STRING);return LIT_STRING;}
 
 <STRING>.				{ *s = yytext[0]; s++;}



"/*"					BEGIN(COMMENT);
<COMMENT>"*/"				BEGIN(INITIAL);
<COMMENT>\n				{ ++num_linhas; }
<COMMENT>.


\n					{ ++num_linhas; }
"//".*					
[ \t]
'.'					{ yylval.symbol= hashInsert(yytext,SYMBOL_LIT_CHAR);return LIT_CHAR; }
.					{ return TOKEN_ERROR; }











%%
	
int yywrap() {
		running = 0;
		return 1;
		//  <STRING>\\n 				{ *s++ = '\n'; }
	}
	
int isRunning(void)
{
	return running;
}
