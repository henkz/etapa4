//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771

#include "hash.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


// SYMBOL TABLE

HASH_NODE *Table[HASH_SIZE];  
 
// IMPLEMENTATIONS

void initMe(void)
{
	hashInit();
}

void hashInit(void)
{
	int i;
	for (i = 0 ; i < HASH_SIZE ; i++)
	{
		Table[i]=0;
	}
}
   
int hashAddress(char *text)
{
	int address = 1;
	int i;
	
	for (i =0 ; i < strlen(text) ; i++)
		address = (address * text[i]) % HASH_SIZE + 1;
	 
	return address - 1;
}
  
HASH_NODE *hashFind(char* text)
{
	HASH_NODE *node;
	int address = hashAddress(text);
	
	for (node = Table[address] ; node ; node = node->next)
		if (!strcmp(node->text, text))
			return node;
     
	return 0;
}  
  
HASH_NODE *hashInsert(char *text, int type) 
{
	HASH_NODE *newNode;
	int address = hashAddress(text);
   
	if(newNode = hashFind(text))
	{
		newNode->lineLastUsed= getLineNumber();	
		return newNode;             
	}
	newNode = calloc(1 , sizeof(HASH_NODE)); //TODO: insercao de 'x' e"asdas". Tirar as aspas.
	newNode->type = type;						
	if(type==SYMBOL_LIT_CHAR)
	{
		newNode->text = calloc( 4, sizeof(char));
		
		newNode->text[0] = '\''; 
		newNode->text[1] = text[1]; 
		newNode->text[2] = '\'';  
		newNode->text[3] = '\0'; 
		
	}
	else if (type==SYMBOL_LIT_STRING)
	{
		
		newNode->text = calloc( strlen(text) + 3, sizeof(char));
		char* aux = calloc( strlen(text) + 3, sizeof(char));
		strcpy(aux, "\"");
		strcat(aux,text);
		strcat(aux,"\"");		
		
		strcpy(newNode->text, aux);
		
		//printf("Imprimindo do hash %s\n",newNode->text);
	}
	else
	
	{	  
		newNode->text = calloc( strlen(text) + 1, sizeof(char));
		strcpy(newNode->text, text);
	
	}
	newNode->next = Table[address];
	Table[address] = newNode;
	newNode->lineLastUsed= getLineNumber();
	
   
	return newNode;
}
    
void hashPrint(void)
{
	int i;
	HASH_NODE *node;
	
	for(i = 0 ; i < HASH_SIZE ; i++)
		for (node = Table[i]; node ; node = node->next)
			printf("table[%d]=%s\n", i, node->text);
			
}


void checkUndeclared(void)
{
	int i;
	HASH_NODE *node = 0;

	for (i=0; i<HASH_SIZE; ++i) 
		for (node = Table[i]; node; node = node->next)
			if (node->type == SYMBOL_IDENTIFIER)
				fprintf(stderr,"Symbol %s undeclared.\n",node->text);
}
      
// END OF FILE
 
