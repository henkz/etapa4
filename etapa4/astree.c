//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771
#include "astree.h"


ASTREE* astreeCreate(int type, HASH_NODE *symbol, ASTREE* son0, ASTREE* son1, ASTREE* son2, ASTREE* son3) {

	ASTREE *newNode = 0;
	newNode = (ASTREE*) calloc(1,sizeof(ASTREE));
	newNode->type = type;
	newNode->symbol = symbol;
	newNode->son[0] = son0;
	newNode->son[1] = son1;
	newNode->son[2] = son2;
	newNode->son[3] = son3;
	newNode->line_number = getLineNumber();
	
	return newNode;
}

void astreePrint(ASTREE* node, int level) {

	int i;
	if(!node)
		return;
	
	for(i=0 ; i<level ; ++i)
		fprintf(stderr, " ");
	fprintf(stderr,"Astree(");
	
	switch (node->type) {
		
		case ASTREE_UNDEFINED: fprintf(stderr,"UNDEFINED"); break;
		case ASTREE_SYMBOL: fprintf(stderr,"ASTREE_SYMBOL"); break;
		case ASTREE_LE: fprintf(stderr,"ASTREE_LE"); break;
		case ASTREE_GE: fprintf(stderr,"ASTREE_GE"); break;
		case ASTREE_EQ: fprintf(stderr,"ASTREE_EQ"); break;
		case ASTREE_NE: fprintf(stderr,"ASTREE_NE"); break;
		case ASTREE_AND: fprintf(stderr,"ASTREE_AND"); break;
		case ASTREE_OR: fprintf(stderr,"ASTREE_OR"); break;
		case ASTREE_ADD: fprintf(stderr,"ASTREE_ADD"); break;
		case ASTREE_SUB: fprintf(stderr,"ASTREE_SUB"); break;
		case ASTREE_MUL: fprintf(stderr,"ASTREE_MUL"); break;
		case ASTREE_DIV: fprintf(stderr,"ASTREE_DIV"); break;
		case ASTREE_GRT: fprintf(stderr,"ASTREE_GRT"); break;
		case ASTREE_LESS: fprintf(stderr,"ASTREE_LESS"); break;
		case ASTREE_NEG: fprintf(stderr,"ASTREE_NEG"); break;
		case ASTREE_LCMD: fprintf(stderr,"ASTREE_LCMD"); break;
		case ASTREE_ASS: fprintf(stderr,"ASTREE_ASS"); break;
		case ASTREE_IF: fprintf(stderr,"ASTREE_IF"); break;
		case ASTREE_IFTE: fprintf(stderr,"ASTREE_IFTE"); break;
		case ASTREE_EXP_PAR: fprintf(stderr,"ASTREE_EXP_PAR"); break; 
		case ASTREE_VEC: fprintf(stderr,"ASTREE_VEC"); break;
		case ASTREE_ASS_VEC: fprintf(stderr,"ASTREE_ASS_VEC"); break;
		case ASTREE_WHILE: fprintf(stderr,"ASTREE_WHILE"); break;
		case ASTREE_INPUT: fprintf(stderr,"ASTREE_INPUT"); break;
		case ASTREE_LIST_IDENTIFIER: fprintf(stderr,"ASTREE_LIST_IDENTIFIER"); break;		
		case ASTREE_OUTPUT: fprintf(stderr,"ASTREE_OUTPUT"); break;
		case ASTREE_LIST_OUTPUT: fprintf(stderr,"ASTREE_LIST_OUTPUT"); break;
		case ASTREE_RET: fprintf(stderr,"ASTREE_RET"); break;
		case ASTREE_NOP: fprintf(stderr,"ASTREE_NOP"); break;		
		case ASTREE_CMD_LIST: fprintf(stderr,"ASTREE_CMD_LIST"); break;
		case ASTREE_CMD_BLOCK: fprintf(stderr,"ASTREE_CMD_BLOCK"); break;
		case ASTREE_DECL_FUNC: fprintf(stderr,"ASTREE_DECL_FUNC"); break;
		case ASTREE_DECL_FUNC_WO_PARAM: fprintf(stderr,"ASTREE_DECL_FUNC_WO_PARAM"); break;
		case ASTREE_LIST_PARAM: fprintf(stderr,"ASTREE_LIST_PARAM"); break;
		
		case ASTREE_KW_INT: fprintf(stderr,"ASTREE_KW_INT"); break;
		case ASTREE_KW_REAL: fprintf(stderr,"ASTREE_KW_REAL"); break;
		case ASTREE_KW_BOOL: fprintf(stderr,"ASTREE_KW_BOOL"); break;
		case ASTREE_KW_CHAR: fprintf(stderr,"ASTREE_KW_CHAR"); break;
		
		case ASTREE_FUNC_CALL_WO_PARAM: fprintf(stderr,"ASTREE_FUNC_CALL_WO_PARAM"); break;
		case ASTREE_FUNC_CALL: fprintf(stderr,"ASTREE_FUNC_CALL"); break;
		case ASTREE_ARG_LIST: fprintf(stderr,"ASTREE_ARG_LIST"); break;
		
		case ASTREE_DECL_VAR: fprintf(stderr,"ASTREE_DECL_VAR"); break;
		case ASTREE_DECL_VEC: fprintf(stderr,"ASTREE_DECL_VEC "); break;
		case ASTREE_DECL_VEC_INIT: fprintf(stderr,"ASTREE_DECL_VEC_INIT"); break;
		case ASTREE_LIST_LIT_VALUE: fprintf(stderr,"ASTREE_LIST_LIT_VALUE"); break;
		
		
			


		
		
		default: fprintf(stderr,"UNKNOWN"); break;
	}
	if(node->symbol)
		fprintf(stderr,", %s )",node->symbol->text);
	
	fprintf(stderr,"\n");
	
	for(i=0 ; i<MAX_SONS ; ++i)
		astreePrint(node->son[i],level+1);
}





void astreePrintFile(ASTREE* node, FILE* ofile) {

	int i;
	
	//ofile =stderr;
	
	
	if(!node)
		return;
	
	//fprintf(stderr,"Astree(");
	
	switch (node->type) {
		
		case ASTREE_UNDEFINED: fprintf(stderr,"UNDEFINED"); break;
		case ASTREE_SYMBOL: 
			fprintf(ofile,"%s ",node->symbol->text);						
			break;
		case ASTREE_LE: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"<=");	
			astreePrintFile(node->son[1],  ofile);
			break;
		case ASTREE_GE: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,">=");	
			astreePrintFile(node->son[1],  ofile);
			break;
		case ASTREE_EQ: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"==");	
			astreePrintFile(node->son[1], ofile);
			break;
		case ASTREE_NE: 
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"!=");	
			astreePrintFile(node->son[1],  ofile);
			break;
		case ASTREE_AND:
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"&&");	
			astreePrintFile(node->son[1], ofile);
			break;
		case ASTREE_OR:  
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"||");	
			astreePrintFile(node->son[1],  ofile);
			break;
		case ASTREE_ADD: 
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"+");	
			astreePrintFile(node->son[1], ofile);
			break;
		case ASTREE_SUB: 
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"-");	
			astreePrintFile(node->son[1],  ofile);
			 break;
		case ASTREE_MUL:  
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"*");	
			astreePrintFile(node->son[1],  ofile);
			 break;
		case ASTREE_DIV:  
			astreePrintFile(node->son[0],  ofile);
			fprintf(ofile,"/");	
			astreePrintFile(node->son[1], ofile);
			 break;
		case ASTREE_GRT: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,">");	
			astreePrintFile(node->son[1],  ofile);
			 break;
		case ASTREE_LESS:
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"<");	
			astreePrintFile(node->son[1],  ofile);
			 break;
		case ASTREE_NEG: 
			fprintf(ofile,"!");	
			astreePrintFile(node->son[0], ofile);
			break;
		///case ASTREE_LCMD: fprintf(stderr,"ASTREE_LCMD"); break;//TODO
		case ASTREE_ASS: 
			astreePrintFile(node->son[0], ofile); 
			fprintf(ofile,"=");
			astreePrintFile(node->son[1], ofile); 	
			break;
		 
		case ASTREE_IF:
			fprintf(ofile,"if (");
			astreePrintFile(node->son[0], ofile); 
			fprintf(ofile,")\n");
			astreePrintFile(node->son[1], ofile); 
			fprintf(ofile,"\n");
			break;
		case ASTREE_IFTE:
			fprintf(ofile,"if (");
			astreePrintFile(node->son[0], ofile); 
			fprintf(ofile,")\n");
			astreePrintFile(node->son[1], ofile); 
			fprintf(ofile,"\nelse\n");
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile,"\n");
			break;
		case ASTREE_EXP_PAR: 
			fprintf(ofile,"(");
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,")");
			break; 
		case ASTREE_VEC: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," [");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile,"] ");
			 break;
		case ASTREE_ASS_VEC: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," [");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile,"] = ");
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile," ");
			break;
			 
		case ASTREE_WHILE: 
			fprintf(ofile,"while (");
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,")\n");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile,"\n");
			break;
		case ASTREE_INPUT:
			fprintf(ofile,"\ninput ");
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"\n");
			 break;
		case ASTREE_LIST_IDENTIFIER: 
			astreePrintFile(node->son[0], ofile);
			if(node->son[1]!=0)		
			{
				fprintf(ofile,", ");
				astreePrintFile(node->son[1], ofile);
			}
			break;		
		case ASTREE_OUTPUT:
			fprintf(ofile,"\noutput ");
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"\n");
			break;
		case ASTREE_LIST_OUTPUT:  
			astreePrintFile(node->son[0], ofile);
			if(node->son[1]!=0)		
			{
				fprintf(ofile,", ");
				astreePrintFile(node->son[1], ofile);
			}
			fprintf(ofile," ");
			break;
		case ASTREE_RET: 
			 fprintf(ofile,"\nreturn ");
			 astreePrintFile(node->son[0], ofile);
			 fprintf(ofile,"\n");
			 break;
		case ASTREE_NOP: 
			fprintf(ofile,"\n;\n");
			break;		
		case ASTREE_CMD_LIST: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"\n");
			astreePrintFile(node->son[1], ofile);
			break;
		case ASTREE_CMD_BLOCK: 
			fprintf(ofile,"{\n");
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile,"}\n");
			 break;
		case ASTREE_DECL_FUNC: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile," (");
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile," )\n");
			fprintf(ofile," ");
			astreePrintFile(node->son[3], ofile);
			fprintf(ofile," ; ");
			break;
			
		case ASTREE_DECL_FUNC_WO_PARAM: 
			fprintf(ofile," ");
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile," ()\n");		
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile," ; ");
			break;
		case ASTREE_LIST_PARAM:
			fprintf(ofile," ");
			astreePrintFile(node->son[0], ofile);
			astreePrintFile(node->son[1], ofile);
			if(node->son[2]!=0)		
			{
				fprintf(ofile,", ");
				astreePrintFile(node->son[2], ofile);
			}
	
			break;		
		case ASTREE_KW_INT:
			fprintf(ofile," int ");
			break;
		case ASTREE_KW_REAL: 
			fprintf(ofile," real ");
			 break;
		case ASTREE_KW_BOOL: 
			fprintf(ofile," bool ");
			 break;
		case ASTREE_KW_CHAR: 
			fprintf(ofile," char ");
			break;
		
		case ASTREE_FUNC_CALL_WO_PARAM:
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," () ");
			break;
		case ASTREE_FUNC_CALL:  
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ( ");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile," ) ");
			break;		
		
		case ASTREE_ARG_LIST: 
			astreePrintFile(node->son[0], ofile);
			if(node->son[1]!=0)		
			{
				fprintf(ofile,", ");
				astreePrintFile(node->son[1], ofile);
			}
			break;		
		case ASTREE_DECL_VAR:
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile,":");
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile,";\n");		
			break;
		case ASTREE_DECL_VEC:
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile,"[");
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile,"];\n ");		
			break;
		case ASTREE_DECL_VEC_INIT:
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ");
			astreePrintFile(node->son[1], ofile);
			fprintf(ofile,"[");
			astreePrintFile(node->son[2], ofile);
			fprintf(ofile,"]:");
			astreePrintFile(node->son[3], ofile);
			fprintf(ofile,";\n");	
			break;
		case ASTREE_LIST_LIT_VALUE: 
			astreePrintFile(node->son[0], ofile);
			fprintf(ofile," ");
			astreePrintFile(node->son[1], ofile);
			break;	
		
		
		default: fprintf(stderr,"UNKNOWN"); break;
	}
	/*if(node->symbol)
		fprintf(stderr,", %s )",node->symbol->text);
	
	fprintf(stderr,"\n");
	
	for(i=0 ; i<MAX_SONS ; ++i)
		astreePrint(node->son[i],level+1);
*/}
