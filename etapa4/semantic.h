//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771

#ifndef SEMANTIC_HEADER
#define SEMANTIC_HEADER 0

#include "hash.h"
#include "astree.h"
#include <stdio.h>
#include <stdlib.h>

void checkDeclaration(ASTREE* node);
void checkUsage(ASTREE* node);
void checkDataType(ASTREE* node);

#endif
