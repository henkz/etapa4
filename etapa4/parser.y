//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771

%{
	#include <stdio.h>
	#include <stdlib.h>
	//#include "y.tab.h" 
	#include "hash.h"
	#include "astree.h"
	
	extern FILE* out;
%}
	
%union
{
	struct hash_node_struct * symbol;//HASH_NODE *symbol;
	struct astree_node * astree;//ASTREE *astree;
};

	

	
%token KW_INT	
%token KW_REAL
%token KW_BOOL
%token KW_CHAR
%token KW_IF
%token KW_ELSE
%token KW_WHILE
%token KW_INPUT
%token KW_RETURN
%token KW_OUTPUT
%token OPERATOR_LE
%token OPERATOR_GE
%token OPERATOR_EQ
%token OPERATOR_NE
%token OPERATOR_AND
%token OPERATOR_OR
%token<symbol> TK_IDENTIFIER
%token<symbol> LIT_INTEGER
%token<symbol> LIT_FALSE
%token<symbol> LIT_TRUE
%token<symbol> LIT_CHAR
%token<symbol> LIT_STRING
%token TOKEN_ERROR

	
%type<astree> program
%type<astree> var_type	
%type<astree> lit_value
%type<astree> decl_var
%type<astree> list_lit_value
%type<astree> decl_func
%type<astree> list_identifiers
%type<astree> func_call
%type<astree> argument_list
%type<astree> decl_param_list
%type<astree> command
%type<astree> cmd_return
%type<astree> cmd_list
%type<astree> cmd_assignment
%type<astree> cmd_if
%type<astree> cmd_while
%type<astree> cmd_input
%type<astree> cmd_output
%type<astree> list_output
%type<astree> expression

					
		
	
%left '>' '<' OPERATOR_OR OPERATOR_AND OPERATOR_NE OPERATOR_EQ OPERATOR_GE OPERATOR_LE
%left '+' '-'
%left '*' '/'
%left '!'
	
%%

program:program decl_var 				{checkDeclaration($2);checkUsage($2);}
	|decl_var					{checkDeclaration($1);checkUsage($1);}
	|decl_func					{checkDeclaration($1);checkUsage($1);}
 	|program decl_func 				{checkDeclaration($2);checkUsage($2);}
	;
	
var_type: KW_INT					{$$ = astreeCreate(ASTREE_KW_INT,0,0,0,0,0);}
	| KW_REAL					{$$ = astreeCreate(ASTREE_KW_REAL,0,0,0,0,0);}
	| KW_CHAR					{$$ = astreeCreate(ASTREE_KW_CHAR,0,0,0,0,0);}
	| KW_BOOL					{$$ = astreeCreate(ASTREE_KW_BOOL,0,0,0,0,0);}
 ; 
 
lit_value: LIT_INTEGER 					{$$=astreeCreate(ASTREE_LITERAL,$1,0,0,0,0);}
    |LIT_TRUE						{$$=astreeCreate(ASTREE_LITERAL,$1,0,0,0,0);}
    |LIT_FALSE						{$$=astreeCreate(ASTREE_LITERAL,$1,0,0,0,0);}
    | LIT_CHAR						{$$=astreeCreate(ASTREE_LITERAL,$1,0,0,0,0);}
    ;

 

decl_var:var_type TK_IDENTIFIER ':' lit_value';'			{$$ = astreeCreate(ASTREE_DECL_VAR,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),$4,0);}
	|var_type TK_IDENTIFIER'['LIT_INTEGER']' ';'			{$$ = astreeCreate(ASTREE_DECL_VEC,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),astreeCreate(ASTREE_SYMBOL,$4,0,0,0,0),0);}
	|var_type TK_IDENTIFIER'['LIT_INTEGER']' ':' list_lit_value';'	{$$ = astreeCreate(ASTREE_DECL_VEC_INIT,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),astreeCreate(ASTREE_SYMBOL,$4,0,0,0,0),$7);}
	;
list_lit_value:lit_value list_lit_value			{$$=astreeCreate(ASTREE_LIST_LIT_VALUE,0,$1,$2,0,0);}
	|lit_value					{$$=astreeCreate(ASTREE_LIST_LIT_VALUE,0,$1,0,0,0);}
	; 


list_identifiers: TK_IDENTIFIER ',' list_identifiers    {$$ = astreeCreate(ASTREE_LIST_IDENTIFIER,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,0,0);}
		|TK_IDENTIFIER				{$$ = astreeCreate(ASTREE_LIST_IDENTIFIER,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),0,0,0);}
		;
 
 
decl_func:var_type TK_IDENTIFIER '(' decl_param_list')'	command ';'	{$$ = astreeCreate(ASTREE_DECL_FUNC,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),$4,$6);}
	|var_type TK_IDENTIFIER '('')'	command ';'			{$$ = astreeCreate(ASTREE_DECL_FUNC_WO_PARAM,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),$5,0);}
	;
	
func_call:TK_IDENTIFIER '('')' 				{$$ = astreeCreate(ASTREE_FUNC_CALL_WO_PARAM,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),0,0,0);}
	 |TK_IDENTIFIER '('argument_list')' 		{$$ = astreeCreate(ASTREE_FUNC_CALL,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,0,0);}
	 ;
	 
argument_list:TK_IDENTIFIER ',' argument_list		{$$ = astreeCreate(ASTREE_ARG_LIST,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,0,0);}
	 |TK_IDENTIFIER					{$$ = astreeCreate(ASTREE_ARG_LIST,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),0,0,0);}
	 |lit_value ',' argument_list			{$$ = astreeCreate(ASTREE_ARG_LIST,0,$1,$3,0,0);}
	 |lit_value					{$$ = astreeCreate(ASTREE_ARG_LIST,0,$1,0,0,0);}
	;
	 
decl_param_list:var_type TK_IDENTIFIER','decl_param_list	{$$ = astreeCreate(ASTREE_LIST_PARAM,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),$4,0);}
	|var_type TK_IDENTIFIER					{$$ = astreeCreate(ASTREE_LIST_PARAM,0,$1,astreeCreate(ASTREE_SYMBOL,$2,0,0,0,0),0,0);}
	;
command:'{'cmd_list'}'					{$$ = astreeCreate(ASTREE_CMD_BLOCK,0,$2,0,0,0);}
	|cmd_assignment					{$$=$1;}	
	|cmd_if						{$$=$1;}
	|cmd_while					{$$=$1;}
	|cmd_input					{$$=$1;}
	|cmd_output					{$$=$1;}
	|cmd_return					{$$=$1;}
	|';'						{$$ = astreeCreate(ASTREE_NOP,0,0,0,0,0);}
	;	
cmd_return:KW_RETURN expression				{$$ = astreeCreate(ASTREE_RET,0,$2,0,0,0);}
	;

cmd_list:command cmd_list				{$$ = astreeCreate(ASTREE_CMD_LIST,0,$1,$2,0,0);}
	|command 					{$$ = astreeCreate(ASTREE_CMD_LIST,0,$1,0,0,0);}
	;
	
cmd_assignment:TK_IDENTIFIER '=' expression		{$$ = astreeCreate(ASTREE_ASS,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,0,0);}//{$$ = astreeCreate(ASTREE_ASS,$1,$3,0,0,0);}	
	|TK_IDENTIFIER'['expression']' '=' expression	{$$ = astreeCreate(ASTREE_ASS_VEC,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,$6,0);}//{$$ = astreeCreate(ASTREE_ASS_VEC,$1,$3,$6,0,0);}
	;
	
cmd_if:KW_IF '('expression')' command KW_ELSE command	{$$ = astreeCreate(ASTREE_IFTE,0,$3,$5,$7,0);}
	|KW_IF '('expression')' command			{$$ = astreeCreate(ASTREE_IF,0,$3,$5,0,0);}
	;
cmd_while:KW_WHILE '('expression')' command		{$$ = astreeCreate(ASTREE_WHILE,0,$3,$5,0,0);}
	;
	
cmd_input:KW_INPUT list_identifiers			{$$ = astreeCreate(ASTREE_INPUT,0,$2,0,0,0);}
	;
	
cmd_output:KW_OUTPUT list_output			{$$ = astreeCreate(ASTREE_OUTPUT,0,$2,0,0,0);}
	;
	
list_output:LIT_STRING ',' list_output			{$$ = astreeCreate(ASTREE_LIST_OUTPUT,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,0,0);}
	|expression ',' list_output			{$$ = astreeCreate(ASTREE_LIST_OUTPUT,0,$1,$3,0,0);}
	|LIT_STRING					{$$ = astreeCreate(ASTREE_LIST_OUTPUT,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),0,0,0);}
	|expression					{$$ = astreeCreate(ASTREE_LIST_OUTPUT,0,$1,0,0,0);}
	;


											

expression:TK_IDENTIFIER			{ $$ = astreeCreate(ASTREE_JUST_IDENTIFIER,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),0,0,0); }
	|TK_IDENTIFIER'['expression']'		{ $$ = astreeCreate(ASTREE_VEC,0,astreeCreate(ASTREE_SYMBOL,$1,0,0,0,0),$3,0,0); }
	|lit_value				{$$=$1;}	
	|func_call				{ $$ = $1; }
	|'(' expression')'	  		{ $$ = astreeCreate(ASTREE_EXP_PAR,0,$2,0,0,0); }
	| expression OPERATOR_LE expression	{ $$ = astreeCreate(ASTREE_LE,0,$1,$3,0,0); }			
	| expression OPERATOR_GE expression	{ $$ = astreeCreate(ASTREE_GE,0,$1,$3,0,0); }			
	| expression OPERATOR_EQ expression	{ $$ = astreeCreate(ASTREE_EQ,0,$1,$3,0,0); }		
	| expression OPERATOR_NE expression	{ $$ = astreeCreate(ASTREE_NE,0,$1,$3,0,0); }		
	| expression OPERATOR_AND expression	{ $$ = astreeCreate(ASTREE_AND,0,$1,$3,0,0); }		
	| expression OPERATOR_OR expression	{ $$ = astreeCreate(ASTREE_OR,0,$1,$3,0,0); }		
	| expression '+' expression	  	{ $$ = astreeCreate(ASTREE_ADD,0,$1,$3,0,0); }			
	| expression '-' expression	  	{ $$ = astreeCreate(ASTREE_SUB,0,$1,$3,0,0); }	
	| expression '*' expression	   	{ $$ = astreeCreate(ASTREE_MUL,0,$1,$3,0,0); }		
	| expression '/' expression	   	{ $$ = astreeCreate(ASTREE_DIV,0,$1,$3,0,0); }		
	| expression '>' expression	 	{ $$ = astreeCreate(ASTREE_GRT,0,$1,$3,0,0); }		
	| expression '<' expression		{ $$ = astreeCreate(ASTREE_LESS,0,$1,$3,0,0); }		
	|'!' expression				{ $$ = astreeCreate(ASTREE_NEG,0,$2,0,0,0); }
	;


%%

int yyerror() {
	fprintf(stderr, "SYNTAX_ERROR in line: %d\n",getLineNumber());
	exit(3);
}





