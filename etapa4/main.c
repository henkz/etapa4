//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771

#include "hash.h"
#include "astree.h"
#include "y.tab.h"
#include "lex.yy.h"
#include <stdio.h>
#include <stdlib.h>



int main(int argc,char **argv) {
	int tok = 0;
	
	extern FILE *yyin;
	extern int num_linhas;
	
	if(argc<2){
		printf("Call source_file\n");
		exit(1);
	}
	
	
	yyin=fopen(argv[1],"r");
	
	if(yyin==0)
	{
		printf("Could not open source file: %s\n",argv[1]);
		exit(2);
	}
	
	yyparse();
	checkUndeclared();	
	


	printf("==>program accepted!\n");
	
	/*printf("Imprimindo Tabela Hash:\n");
	hashPrint();
	*/

	fclose(yyin);
	//fclose(out);
	//printf("==>Output file writen with success!\n");
	exit(0);	
	
	
}



 
