#include "semantic.h"



//Percorre a AST em busca de todas as declaracoes feitas no programa e atualiza os seus tipos. (funcoes, tipos de variaveis...)
//Detecta redeclaracao quando encontra uma declaracao com um simbolo ja atualizado.

void checkDeclaration(ASTREE* node)
{
	int i;
	ASTREE* aux_node=0;
	if(!node) return;
		
	if(node->type == ASTREE_DECL_VAR ||node->type == ASTREE_LIST_PARAM)
	{
		aux_node=node->son[1];
		if(aux_node->symbol->type!= SYMBOL_IDENTIFIER)
			fprintf(stderr,"SEMANTIC ERROR: Variable %s redeclared.\n", aux_node->symbol->text);
		else
			aux_node->symbol->type = SYMBOL_VAR;
	}
	if(node->type == ASTREE_DECL_VEC ||node->type == ASTREE_DECL_VEC_INIT)
	{
		aux_node=node->son[1];
		if(aux_node->symbol->type!= SYMBOL_IDENTIFIER)
			fprintf(stderr,"SEMANTIC ERROR: Variable %s redeclared.\n", aux_node->symbol->text);
		else
			aux_node->symbol->type = SYMBOL_VEC;
	}
	
	if(node->type == ASTREE_DECL_FUNC || node->type == ASTREE_DECL_FUNC_WO_PARAM)
	{	
		aux_node=node->son[1];
		if(aux_node->symbol->type!= SYMBOL_IDENTIFIER)
			fprintf(stderr,"SEMANTIC ERROR: Variable %s redeclared.\n", aux_node->symbol->text);
		else
			aux_node->symbol->type = SYMBOL_FUNC;
	}			
	if(node->type == ASTREE_DECL_VAR ||node->type == ASTREE_DECL_VEC ||node->type == ASTREE_DECL_FUNC ||node->type == ASTREE_DECL_VEC_INIT || node->type == ASTREE_DECL_FUNC_WO_PARAM ||node->type == ASTREE_LIST_PARAM )
	{
		if(node->son[0]->type==ASTREE_TYPEINT)
			node->symbol->datatype = DATATYPE_INT;
		if(node->son[0]->type==ASTREE_TYPEREAL)
			node->symbol->datatype = DATATYPE_REAL;
		if(node->son[0]->type==ASTREE_TYPECHAR)
			node->symbol->datatype = DATATYPE_CHAR;
		if(node->son[0]->type==ASTREE_TYPEBOOL)
			node->symbol->datatype = DATATYPE_BOOL;
			
	}	
	for(i=0;i<MAX_SONS;i++)
		checkDeclaration(node->son[i]);

}

//Checa se as variaveis e funcoes estao sendo usadas como declaradas
void checkUsage(ASTREE* node)
{
	int i;
	ASTREE* aux_node=0;
	
	if(!node) return;
	
	switch(node->type)
	{
		
	//chamada de funcao
	case ASTREE_FUNC_CALL_WO_PARAM: // OU
	case ASTREE_FUNC_CALL:		
		if(!(aux_node=node->son[0]))	{fprintf(stderr,"filho[0] =null"); return;}
		if(aux_node->symbol->type != SYMBOL_FUNC)
			fprintf(stderr,"SEMANTIC ERROR: Symbol %s must be declared as function call \n",aux_node->symbol->text);
		break;
		
	//atribuicoes
	case ASTREE_ASS: 
		
		if(!(aux_node=node->son[0])) {fprintf(stderr,"filho[0] =null"); return;}
		if(aux_node->symbol->type != SYMBOL_VAR)
			fprintf(stderr,"SEMANTIC ERROR: Symbol %s must be declared as scalar \n",aux_node->symbol->text);
		break;
	
	case ASTREE_ASS_VEC: 
		if(!(aux_node=node->son[0])){fprintf(stderr,"filho[0] =null"); return;}
		
		if(aux_node->symbol->type != SYMBOL_VEC)
			fprintf(stderr,"SEMANTIC ERROR: Symbol %s must be declared as vector in line %d \n",aux_node->symbol->text,aux_node->symbol->lineLastUsed);
		break;
	//expressoes
	case ASTREE_JUST_IDENTIFIER:
		if(!(aux_node=node->son[0])) {fprintf(stderr,"filho[0] =null"); return;}
		if(aux_node->symbol->type != SYMBOL_VAR)
			fprintf(stderr,"SEMANTIC ERROR: Symbol %s must be declared as scalar \n",aux_node->symbol->text);
		break;
	case ASTREE_VEC: 
		if(!(aux_node=node->son[0])){fprintf(stderr,"filho[0] =null"); return;}
		if(aux_node->symbol->type != SYMBOL_VEC)
			fprintf(stderr,"SEMANTIC ERROR: Symbol %s must be declared as vector \n",aux_node->symbol->text);
		break;	
	default:
		;
		
	}
	
	for(i=0;i<MAX_SONS;i++)
		checkUsage(node->son[i]);	



}


/*
Tipos    de    dados    
–
As    declarações    também    devem    registrar    os    tipos    de    dados,    em    
um     novo     campo,     dataType,     na     tabela     de     símbolos.     Com     o     auxílio     dessa    
informação,    quando    necessário,    os    tipos    de    dados    corretos    devem    ser    verificados    
onde    forem    usados,    em    expressõs    aritméticas,    rel
acionais,    lógicas,    ou    para    índices    
de    vetores
;

*/
void checkDataType(ASTREE* node)
{
	int i;
	ASTREE* aux_node=0;
	
	if(!node) return;
	
	



}

//retorna o tipo de dados desta subarvore
//so para int e real
int getDataType(ASTREE* node)
{
	int tipoDosFilhos[MAX_SONS];
	int i,retorno;
	if(!node) return -1;
	
	
	switch (node->type)
	case ASTREE_SYMBOL:
		return node->symbol->datatype;
		break;
	//resultado booleano com operandos numericos 
	case ASTREE_EQ:
	case ASTREE_NE:
	case ASTREE_AND:
	case ASTREE_LESS:
	case ASTREE_GRT:
	case ASTREE_GE:
		for(i=0;i<MAX_SONS;i++)
		{
			if(getDataType(node->son[i])==DATATYPE_BOOL);
			fprintf(stderr, "SEMANTIC ERROR: operator must not be boolean\n");///TODO o que retornar quando o tipo eh invalido?
		}	
	return DATATYPE_BOOL;
	
	//case ASTREE_NEG: TODO pois so pode ser usado com expressoes booleanas

	for(i=0;i<MAX_SONS;i++)
	{
		tipoDosFilhos[i]=getDataType(node->son[i]);
	
	}
	
	
	
	
	
	
	


}




/*
ASTREE_LE			
	ASTREE_EQ		
	ASTREE_NE		
	ASTREE_NEG
	ASTREE_AND		
	ASTREE_OR
	ASTREE_LESS
	ASTREE_GRT
	ASTREE_GE		

	ASTREE_ADD		
	ASTREE_SUB
	ASTREE_MUL	
	ASTREE_DIV	
	
	*/




