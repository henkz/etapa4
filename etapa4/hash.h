//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771

#ifndef HASH_HEADER
#define HASH_HEADER 0

// DEFINES

#define HASH_SIZE 997

#define SYMBOL_LIT_INT 1
#define SYMBOL_LIT_REAL 2
#define SYMBOL_LIT_FALSE 3
#define SYMBOL_LIT_TRUE 4
#define SYMBOL_LIT_CHAR 5
#define SYMBOL_LIT_STRING 6
#define SYMBOL_IDENTIFIER 7

#define SYMBOL_VAR 801
#define SYMBOL_VEC 802
#define SYMBOL_FUNC 803



#define DATATYPE_INT 701
#define DATATYPE_REAL 702
#define DATATYPE_BOOL 703
#define DATATYPE_CHAR 704


// DATA STRUCTURE

typedef struct hash_node_struct 
{
	int type;
	char* text;		// lexema
	int datatype;
	int lineLastUsed;
	struct hash_node_struct *next;
 
}HASH_NODE;

// PROTOTYPES
 
void initMe(void); 
void hashInit(void);							// inicializa hash
int hashAddress(char *text);					// 
HASH_NODE *hashFind(char* text);				// procura lexema na tabela hash
HASH_NODE *hashInsert(char *text ,int type);	// insere lexema na tabela hash
void hashPrint(void);							// imprime estrutura
void checkUndeclared(void);
#endif
// END OF FILE
