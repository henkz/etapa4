//Henrique Colao Zanúz - 240492 
//Jéssica Daltrozo Barbosa - 218771

#ifndef ASTREE_HEADER
#define ASTREE_HEADER 0

#include "hash.h"
#include <stdio.h>
#include <stdlib.h>

FILE* out;

#define MAX_SONS 4

#define ASTREE_UNDEFINED 0
#define ASTREE_SYMBOL 1

#define ASTREE_LE 2
#define ASTREE_GE 3
#define ASTREE_EQ 4
#define ASTREE_NE 5
#define ASTREE_AND 6
#define ASTREE_OR 7
#define ASTREE_ADD 8
#define ASTREE_SUB 9
#define ASTREE_MUL 10
#define ASTREE_DIV 11
#define ASTREE_GRT 12
#define ASTREE_LESS 13
#define ASTREE_NEG 14
#define ASTREE_LCMD 15
#define ASTREE_ASS 16
#define ASTREE_IF 17
#define ASTREE_IFTE 18
#define ASTREE_RET 19
#define	ASTREE_EXP_PAR 20
#define ASTREE_VEC 21
#define ASTREE_ASS_VEC 22
#define ASTREE_WHILE 23
#define ASTREE_INPUT 24
#define ASTREE_LIST_IDENTIFIER 25
#define ASTREE_OUTPUT 26
#define ASTREE_LIST_OUTPUT 27
#define ASTREE_NOP 28
#define ASTREE_CMD_LIST 29
#define ASTREE_CMD_BLOCK 30
#define ASTREE_DECL_FUNC 31
#define	ASTREE_DECL_FUNC_WO_PARAM 32
#define	ASTREE_LIST_PARAM 33
#define ASTREE_KW_INT 34
#define ASTREE_KW_REAL 35
#define ASTREE_KW_BOOL 36
#define ASTREE_KW_CHAR 37
#define ASTREE_FUNC_CALL_WO_PARAM 38
#define ASTREE_FUNC_CALL 39
#define ASTREE_ARG_LIST 40
#define ASTREE_DECL_VAR 41
#define ASTREE_DECL_VEC 42
#define ASTREE_DECL_VEC_INIT 43
#define ASTREE_LIST_LIT_VALUE 44


#define ASTREE_TYPEINT 45
#define ASTREE_TYPEREAL 46
#define ASTREE_TYPEBOOL 47
#define ASTREE_TYPECHAR 48

#define ASTREE_LITERAL 49
#define ASTREE_JUST_IDENTIFIER 50


typedef struct astree_node {

	int type;
	struct astree_node* son[MAX_SONS];
	HASH_NODE* symbol;
	int line_number;
} ASTREE;

ASTREE* astreeCreate(int type, HASH_NODE *symbol, ASTREE* son0, ASTREE* son1, ASTREE* son2, ASTREE* son3);
void astreePrint(ASTREE* node, int level);
void astreePrintFile(ASTREE* node, FILE* ofile);
#endif
